package com.example.test03;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class DrawableShapeActivity extends AppCompatActivity implements View.OnClickListener {

    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_drawable_shape);
        view = findViewById(R.id.v_content);
//        椭圆
        findViewById(R.id.btn_oval).setOnClickListener(this);
        //矩形
        findViewById(R.id.btn_rect).setOnClickListener(this);
        // 默认背景设置为圆角矩形
        view.setBackgroundResource(R.drawable.shape_rect_gold);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_oval){
            view.setBackgroundResource(R.drawable.shape_oval_rose);
        }else if(v.getId() == R.id.btn_rect){
            view.setBackgroundResource(R.drawable.shape_rect_gold);
        }

    }
}