package com.example.test02;

import android.os.Bundle;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ActFinishActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_act_finish);
        //设置来的按钮设置监听
        findViewById(R.id.tv_back).setOnClickListener(this);
        //监听返回的按钮
        findViewById(R.id.btn_finish).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        //如果按返回图标或者点击完成都可以返回到页面
        if(R.id.tv_back == v.getId() || v.getId() == R.id.btn_finish){
            finish(); //结束当前页面
        }
    }
}