package com.example.test02;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

/**
 * 获取从外部传入的参数
 */
public class ActReceiveActivity extends AppCompatActivity {

    private TextView receive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_act_receive);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        receive = findViewById(R.id.tv_receive);
        //获取到传入的参数
        Intent intent = getIntent();
        //方式1
//        String requestTime = intent.getStringExtra("request_time");
//        String requestContent = intent.getStringExtra("request_content");
        //方式2
        Bundle extras = intent.getExtras();
        Object requestTime = extras.get("request_time");
        Object requestContent = extras.get("request_content");
        String format = String.format("收到数据，时间为：%s\n,内容：%s", requestTime, requestContent);
        receive.setText(format);
    }
}