package com.example.test02;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

/**
 * 发送端
 */
public class ActSendActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_act_send);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        text = findViewById(R.id.tv_send);
        findViewById(R.id.btn_send).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        //创建意图对象,准备跳转到活动页面
        Intent intent = new Intent(this, ActReceiveActivity.class);
        //方式1： 发送内容
//        intent.putExtra("request_time",String.valueOf(System.currentTimeMillis()));
//        intent.putExtra("request_content",text.getText().toString());
        //方式2：或者以这种方式发送
        //创建包裹
        Bundle bundle = new Bundle();
        bundle.putString("request_time",String.valueOf(System.currentTimeMillis()));
        bundle.putString("request_content",text.getText().toString());
        intent.putExtras(bundle);
        //跳转到指定活动页面
        startActivity(intent);
    }
}