package com.example.test02;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ActResponseActivity extends AppCompatActivity implements View.OnClickListener {

    static final String response = "已经收到数据";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_act_response);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        Intent intent = getIntent();
        String requestTime = intent.getStringExtra("request_time");
        String content = intent.getStringExtra("request_content");

        TextView viewById = findViewById(R.id.tv_request);
        viewById.setText("接收到的数据："+requestTime+"\n"+"内容:"+content);

        findViewById(R.id.btn_response).setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("response_time", System.currentTimeMillis()+""); // 往包裹存入名为response_content的字符串
        bundle.putString("response_content", response);

        intent.putExtras(bundle);
        //携带意图返回到上一个页面
        setResult(ActResponseActivity.RESULT_OK,intent);
        finish();
    }
}